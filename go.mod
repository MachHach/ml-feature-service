module gitlab.com/MachHach/ml-feature-service

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-playground/validator/v10 v10.10.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	github.com/stretchr/testify v1.7.0
	gorm.io/driver/mysql v1.2.3
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.5
)
