# Contribution guide

## Requirements

Recommended

- [Visual Studio Code](https://code.visualstudio.com/download), with [Remote - Containers extension (`ms-vscode-remote.remote-containers`)](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
- [Docker](https://www.docker.com/products/docker-desktop), to run Linux containers
- SSH agent running with SSH keys to the Git repository added to it ([to share Git credentials with development environment](https://code.visualstudio.com/docs/remote/containers#_sharing-git-credentials-with-your-container))

Minimum

- Go 1.16.13
- MySQL 8 or above

Note

- First class support for Linux. Windows support is complimentary.

## Setup

If the recommended requirements are met, then:

1. In VS Code, press F1 -> select *Remote-Containers: Clone Repository in Container Volume...* to create the development environment and enter VS Code devcontainer mode.
2. VS Code will spawn these devcontainers (configured by the project via `.devcontainer/devcontainer.json`):
    - Debian Buster environment with Go 1.16.13
    - MySQL 8 database server
3. Optionally, prepare the service's configuration (i.e. `.env` file), by copying `configs/devcontainer.template.env`.
This file has configuration to connect to the MySQL container.

If only the minimum requirements are met, then:

1. Clone the Git repository.
2. Prepare a running MySQL server.
