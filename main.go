package main

import (
	"os"

	"gitlab.com/MachHach/ml-feature-service/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
