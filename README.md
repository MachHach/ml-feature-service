# ML Feature Service

Feature service, powered by Go.

## Getting started

Before using the service, ensure that there is a running database server.

*For users of Visual Studio Code + Docker, it is possible to bootstrap the service environment and database server by using Visual Studio Code devcontainer feature. To know more, [refer to the setup in the project's contribution guide](./CONTRIBUTING.md#setup).*

The service is configured with environment variables. Using a `.env` file (along side the entry point, e.g. `main.go`) is supported. [Refer to below section for a list of available environment variables and their default values.](#environment-variables)

To initialize tables in the database, run command:

```bash
# connect to database (default="mysql://localhost:3306")
# and automatically create database tables
go run main.go migrate
```

To start the service, run command:

```bash
# listen on address specified with ML_FEAT_SRV_WEB_ADDRESS (default="localhost:8080")
# and connect to database (default="mysql://localhost:3306")
go run main.go serve
```

To get a feature switch, send a GET request to the service:

```bash
# send HTTP GET request with mandatory query string
# to http://localhost:8080/feature
curl "localhost:8080/feature?email=foo@example.com&featureName=abc"
```
```json
{"canAccess":false}
```

To toggle a feature switch, send a POST request to the service:

```bash
# send HTTP POST request with JSON body,
# to http://localhost:8080/feature
curl -X POST localhost:8080/feature -d "{\"featureName\": \"abc\", \"email\": \"foo@example.com\", \"enable\": true}"
```

## Environment variables

Variable|Description|Default
--|--|--
`ML_FEAT_SRV_MYSQL_HOST`|MySQL database host|localhost
`ML_FEAT_SRV_MYSQL_PORT`|MySQL database port|3306
`ML_FEAT_SRV_MYSQL_USER`|MySQL database username|root
`ML_FEAT_SRV_MYSQL_PASS`|MySQL database password|example
`ML_FEAT_SRV_MYSQL_DATABASE`|MySQL database name|ml_feature_service
`ML_FEAT_SRV_WEB_ADDRESS`|Web application address|localhost:8080
