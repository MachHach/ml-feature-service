package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/MachHach/ml-feature-service/internal/config"
	"gitlab.com/MachHach/ml-feature-service/internal/database"
	"gitlab.com/MachHach/ml-feature-service/internal/web"

	log "github.com/sirupsen/logrus"
)

func init() {
	rootCmd.AddCommand(serveCmd)
}

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start a server",
	RunE: func(cmd *cobra.Command, args []string) error {
		c, err := config.NewConfig()
		if err != nil {
			return err
		}
		log.Info("Initialized app config")

		db, err := database.NewMySqlClient(c.MySql)
		if err != nil {
			return err
		}
		log.Info("Initialized database connector")

		e, err := web.NewWebEngine(db)
		if err != nil {
			return err
		}
		log.Info("Initialized web engine")

		log.Infof("Listening and serving HTTP on %s", c.Web.Address)
		if err := e.Run(c.Web.Address); err != nil {
			return err
		}

		return nil
	},
}
