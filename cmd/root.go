package cmd

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:          "ml-feature-service",
	Short:        "ML feature service",
	Version:      "0.0.2",
	SilenceUsage: true, // https://github.com/spf13/cobra/issues/340#issuecomment-243790200
}

func Execute() error {
	return rootCmd.Execute()
}
