package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/MachHach/ml-feature-service/internal/config"
	"gitlab.com/MachHach/ml-feature-service/internal/database"
)

func init() {
	rootCmd.AddCommand(migrateCmd)
}

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Perform database migration",
	Args:  cobra.ExactArgs(0),
	RunE: func(cmd *cobra.Command, args []string) error {
		c, err := config.NewConfig()
		if err != nil {
			return err
		}
		db, err := database.NewMySqlClient(c.MySql)
		if err != nil {
			return err
		}

		if err := database.AutoMigrate(db); err != nil {
			return err
		}
		fmt.Fprintln(cmd.OutOrStdout(), "Performed automatic database migrations")

		return nil
	},
}
