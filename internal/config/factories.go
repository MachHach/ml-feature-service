package config

import (
	"fmt"
	"os"

	"github.com/go-playground/validator/v10"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/MachHach/ml-feature-service/internal/utils"
)

func NewConfig() (c *Config, err error) {
	viperCfg := viper.New()

	// default values
	viperCfg.SetDefault("ML_FEAT_SRV_MYSQL_HOST", "localhost")
	viperCfg.SetDefault("ML_FEAT_SRV_MYSQL_PORT", 3306)
	viperCfg.SetDefault("ML_FEAT_SRV_MYSQL_USER", "root")
	viperCfg.SetDefault("ML_FEAT_SRV_MYSQL_PASS", "example")
	viperCfg.SetDefault("ML_FEAT_SRV_MYSQL_DATABASE", "ml_feature_service")
	viperCfg.SetDefault("ML_FEAT_SRV_WEB_ADDRESS", "localhost:8080")

	// read from environment variables
	viperCfg.AutomaticEnv()

	// read from configuration file
	viperCfg.SetConfigFile(".env")
	viperCfg.AddConfigPath(".")

	if err := viperCfg.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Infof("No app config file found")
		} else if _, ok := err.(*os.PathError); ok {
			log.Infof("No app config file found")
		} else {
			// return nil, err
			return c, err
		}
	}

	c = &Config{
		MySql: &MySqlConfig{
			Address: fmt.Sprintf(
				"%s:%d",
				viperCfg.GetString("ML_FEAT_SRV_MYSQL_HOST"),
				viperCfg.GetInt("ML_FEAT_SRV_MYSQL_PORT"),
			),
			Username: viperCfg.GetString("ML_FEAT_SRV_MYSQL_USER"),
			Password: viperCfg.GetString("ML_FEAT_SRV_MYSQL_PASS"),
			Database: viperCfg.GetString("ML_FEAT_SRV_MYSQL_DATABASE"),
		},
		Web: &WebConfig{
			Address: viperCfg.GetString("ML_FEAT_SRV_WEB_ADDRESS"),
		},
	}

	validate := validator.New()
	if err := utils.ValidateStruct(validate, c); err != nil {
		return c, err
	}

	return
}
