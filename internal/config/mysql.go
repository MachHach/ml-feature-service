package config

type MySqlConfig struct {
	Address  string `validate:"required,hostname_port"`
	Username string `validate:"required"`
	Password string
	Database string `validate:"required"`
}
