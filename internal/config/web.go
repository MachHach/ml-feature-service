package config

type WebConfig struct {
	Address string `validate:"required,hostname_port"`
}
