package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewConfig(t *testing.T) {
	actual, err := NewConfig()
	assert.Nil(t, err)
	assert.NotNil(t, actual)
}
