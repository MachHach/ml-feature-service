package config

// Available validator tags:
// https://pkg.go.dev/github.com/go-playground/validator/v10

type Config struct {
	MySql *MySqlConfig
	Web   *WebConfig
}
