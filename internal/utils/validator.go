package utils

import (
	"github.com/go-playground/validator/v10"
)

func NewGinBindValidator() *validator.Validate {
	validate := validator.New()
	validate.SetTagName("binding")
	return validate
}

func ValidateStruct(validate *validator.Validate, s interface{}) error {
	if err := validate.Struct(s); err != nil {
		return &ValidationError{
			Errs: err.(validator.ValidationErrors),
		}
	}
	return nil
}
