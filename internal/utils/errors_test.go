package utils

import (
	"errors"
	"fmt"
	"reflect"
	"testing"

	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

const (
	fieldErrMsg = "Key: '%s' Error:Field validation for '%s' failed on the '%s' tag"
)

type fieldError struct {
	tag            string
	actualTag      string
	ns             string
	structNs       string
	fieldLen       uint8
	structfieldLen uint8
	value          interface{}
	param          string
	kind           reflect.Kind
	typ            reflect.Type
}

func (fe *fieldError) Tag() string {
	return fe.tag
}

func (fe *fieldError) ActualTag() string {
	return fe.actualTag
}

func (fe *fieldError) Namespace() string {
	return fe.ns
}

func (fe *fieldError) StructNamespace() string {
	return fe.structNs
}

func (fe *fieldError) StructField() string {
	return fe.structNs[len(fe.structNs)-int(fe.structfieldLen):]
}

func (fe *fieldError) Value() interface{} {
	return fe.value
}

func (fe *fieldError) Param() string {
	return fe.param
}

func (fe *fieldError) Field() string {
	return fe.ns[len(fe.ns)-int(fe.fieldLen):]
}

func (fe *fieldError) Kind() reflect.Kind {
	return fe.kind
}

func (fe *fieldError) Type() reflect.Type {
	return fe.typ
}

func (fe *fieldError) Error() string {
	return fmt.Sprintf(fieldErrMsg, fe.ns, fe.Field(), fe.tag)
}

func (fe *fieldError) Translate(ut ut.Translator) string {
	return ""
}

var typeString reflect.Type

func init() {
	typeString = reflect.TypeOf("")
}

func TestIsErrorDatabaseRecordNotFound(t *testing.T) {
	assert.True(t, IsErrorDatabaseRecordNotFound(gorm.ErrRecordNotFound))
	assert.False(t, IsErrorDatabaseRecordNotFound(errors.New("some arbitrary error")))
}

func TestInputBindJsonError(t *testing.T) {
	expected := "unable to convert JSON request to valid parameters, error: foo bar"
	err := InputBindJsonError{Err: fmt.Errorf("foo bar")}
	assert.Equal(t, expected, err.Error())
}

func TestInputBindQueryError(t *testing.T) {
	expected := "unable to convert request query to valid parameters, error: foo bar"
	err := InputBindQueryError{Err: fmt.Errorf("foo bar")}
	assert.Equal(t, expected, err.Error())
}

func TestValidationErrorError(t *testing.T) {
	testCases := []struct {
		desc     string
		errs     validator.ValidationErrors
		expected string
	}{
		{
			"no errors",
			validator.ValidationErrors{},
			"encountered 0 validation error(s)",
		},
		{
			"two errors",
			validator.ValidationErrors{
				&fieldError{tag: "tagA", structNs: "nsA", typ: typeString},
				&fieldError{tag: "tagB", structNs: "nsB", typ: typeString},
			},
			"encountered 2 validation error(s)\n- nsA (string) tagA\n- nsB (string) tagB",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			err := ValidationError{Errs: tc.errs}
			actual := err.Error()
			assert.Equal(t, tc.expected, actual)
		})
	}
}
