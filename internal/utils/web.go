package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func AbortJsonFromBindJSON(c *gin.Context, err error) {
	AbortJsonFromErrorObject(c, http.StatusBadRequest, &InputBindJsonError{Err: err})
}

func AbortJsonFromBindQuery(c *gin.Context, err error) {
	AbortJsonFromErrorObject(c, http.StatusBadRequest, &InputBindQueryError{Err: err})
}

func AbortJsonFromErrorObject(c *gin.Context, code int, err error) {
	AbortJsonFromErrorString(c, code, err.Error())
}

func AbortJsonFromErrorString(c *gin.Context, code int, msg string) {
	c.AbortWithStatusJSON(code, NewErrorBody(code, msg))
}

func NewErrorBody(code int, msg string) gin.H {
	return gin.H{
		"error": gin.H{
			"code":    code,
			"message": msg,
		},
	}
}
