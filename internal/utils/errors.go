package utils

import (
	"errors"
	"fmt"
	"strings"

	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func IsErrorDatabaseRecordNotFound(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}

type InputBindJsonError struct {
	Err error
}

func (e *InputBindJsonError) Error() string {
	return fmt.Sprintf("unable to convert JSON request to valid parameters, error: %s", e.Err.Error())
}

type InputBindQueryError struct {
	Err error
}

func (e *InputBindQueryError) Error() string {
	return fmt.Sprintf("unable to convert request query to valid parameters, error: %s", e.Err.Error())
}

type ValidationError struct {
	Errs validator.ValidationErrors
}

func (e *ValidationError) Error() string {
	msgs := []string{}
	for _, err := range e.Errs {
		msgs = append(msgs, fmt.Sprintf(
			"- %s (%s) %s",
			err.StructNamespace(),
			err.Type(),
			err.Tag(),
		))
	}

	msg := fmt.Sprintf("encountered %d validation error(s)", len(msgs))
	result := []string{msg}
	result = append(result, msgs...)
	return strings.Join(result, "\n")
}
