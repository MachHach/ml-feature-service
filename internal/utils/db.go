package utils

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func NewSqliteMemoryDb(opts ...gorm.Option) (*gorm.DB, error) {
	return gorm.Open(sqlite.Open("file::memory:"), opts...)
}
