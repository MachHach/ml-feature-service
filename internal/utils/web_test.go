package utils

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestNewErrorBody(t *testing.T) {
	expected := gin.H{
		"error": gin.H{
			"code":    123,
			"message": "foo bar",
		},
	}
	actual := NewErrorBody(123, "foo bar")
	assert.Equal(t, expected, actual)
}
