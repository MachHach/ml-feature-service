package models

import "gorm.io/gorm"

type SwitchGetQuery struct {
	FeatureName string `form:"featureName" binding:"required"`
	Email       string `form:"email" binding:"required,email"`
}

type SwitchCreationJson struct {
	FeatureName string `json:"featureName" binding:"required"`
	Email       string `json:"email" binding:"required,email"`
	Enabled     *bool  `json:"enable" binding:"required"`
}

type StorableSwitch struct {
	gorm.Model
	FeatureName string `gorm:"column:feature_name;not null;index:idx,priority:1"`
	Email       string `gorm:"column:email;not null;index:idx,priority:2"`
	Enabled     bool   `gorm:"column:enabled;not null"`
}

func (StorableSwitch) TableName() string {
	return "switches"
}
