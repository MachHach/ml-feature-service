package transform

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/MachHach/ml-feature-service/internal/models"
)

func TestInputToStorableSwitch(t *testing.T) {
	enabled := true
	input := models.SwitchCreationJson{
		FeatureName: "f1",
		Email:       "foo@bar.com",
		Enabled:     &enabled,
	}
	st := InputToStorableSwitch(input)
	assert.Equal(t, input.FeatureName, st.FeatureName)
	assert.Equal(t, input.Email, st.Email)
	assert.Equal(t, *input.Enabled, st.Enabled)
}
