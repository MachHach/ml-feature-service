package transform

import "gitlab.com/MachHach/ml-feature-service/internal/models"

func InputToStorableSwitch(input models.SwitchCreationJson) models.StorableSwitch {
	return models.StorableSwitch{
		FeatureName: input.FeatureName,
		Email:       input.Email,
		Enabled:     *input.Enabled,
	}
}
