package web

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/MachHach/ml-feature-service/internal/database"
	"gitlab.com/MachHach/ml-feature-service/internal/models"
	"gitlab.com/MachHach/ml-feature-service/internal/utils"
	"gorm.io/gorm"
)

func setup() (*gin.Engine, *gorm.DB, error) {
	db, err := utils.NewSqliteMemoryDb(&gorm.Config{})
	if err != nil {
		return nil, db, err
	}
	if err := database.AutoMigrate(db); err != nil {
		return nil, db, err
	}

	r, err := NewWebEngine(db)
	return r, db, err
}

func TestNewWebEngine(t *testing.T) {
	actual, err := NewWebEngine(nil)
	assert.Nil(t, err)
	assert.NotNil(t, actual)
}

func TestFeatureGet(t *testing.T) {
	r, db, err := setup()
	if ok := assert.Nil(t, err); !ok {
		return
	}
	db.Create(&models.StorableSwitch{
		FeatureName: "abc",
		Email:       "foo@example.com",
		Enabled:     true,
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/feature", nil)
	q := req.URL.Query()
	for k, v := range map[string]string{
		"email":       "foo@example.com",
		"featureName": "abc",
	} {
		q.Add(k, v)
	}
	req.URL.RawQuery = q.Encode()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "{\"canAccess\":true}", w.Body.String())
}

func TestFeaturePost(t *testing.T) {
	r, _, err := setup()
	if ok := assert.Nil(t, err); !ok {
		return
	}

	payload, err := json.Marshal(gin.H{
		"featureName": "abc",
		"email":       "foo@example.com",
		"enable":      true,
	})
	if ok := assert.Nil(t, err); !ok {
		return
	}
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/feature", bytes.NewBuffer(payload))
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}
