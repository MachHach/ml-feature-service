package web

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/MachHach/ml-feature-service/internal/web/handlers"
	"gorm.io/gorm"
)

func NewWebEngine(db *gorm.DB) (*gin.Engine, error) {
	r := gin.New()

	// Logger middleware will write the logs to gin.DefaultWriter even if you set with GIN_MODE=release.
	r.Use(gin.Logger())

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	r.Use(gin.Recovery())

	// https://pkg.go.dev/github.com/gin-gonic/gin#readme-don-t-trust-all-proxies
	if err := r.SetTrustedProxies(nil); err != nil {
		return r, err
	}

	r.GET("/feature", handlers.GetFeatureSwitch(db))
	r.POST("/feature", handlers.CreateFeatureSwitch(db))

	return r, nil
}
