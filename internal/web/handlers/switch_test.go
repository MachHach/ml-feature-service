package handlers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/MachHach/ml-feature-service/internal/database"
	"gitlab.com/MachHach/ml-feature-service/internal/models"
	"gitlab.com/MachHach/ml-feature-service/internal/utils"
	"gorm.io/gorm"
)

type errorResponse struct {
	Error errorResponseError `json:"error"`
}

type errorResponseError struct {
	Code int `json:"code"`
}

type featureGetResponse struct {
	CanAccess bool `json:"canAccess"`
}

func setup() (*gin.Engine, *gorm.DB, error) {
	db, err := utils.NewSqliteMemoryDb(&gorm.Config{})
	if err != nil {
		return nil, db, err
	}
	if err := database.AutoMigrate(db); err != nil {
		return nil, db, err
	}

	r := gin.Default()
	r.GET("/feature", GetFeatureSwitch(db))
	r.POST("/feature", CreateFeatureSwitch(db))

	return r, db, nil
}

func TestGet(t *testing.T) {
	testCases := []struct {
		desc        string
		email       string
		featureName string
		expect      bool
	}{
		{
			"feature A email 1 enabled",
			"alan@example.com",
			"aa",
			true,
		},
		{
			"feature A email 2 disabled",
			"bob@example.com",
			"aa",
			false,
		},
		{
			"feature B email 1 enabled",
			"cindy@example.com",
			"cc",
			true,
		},
		{
			"feature B email 2 disabled",
			"danny@example.com",
			"cc",
			false,
		},
		{
			"feature exists, email not exists",
			"zzz@example.com",
			"aa",
			false,
		},
		{
			"feature not exists, email exists",
			"alan@example.com",
			"zz",
			false,
		},
		{
			"feature and email not exists",
			"zzz@example.com",
			"zz",
			false,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			r, db, err := setup()
			if ok := assert.Nil(t, err); !ok {
				return
			}
			for _, row := range []models.StorableSwitch{
				{FeatureName: "aa", Email: "alan@example.com", Enabled: true},
				{FeatureName: "aa", Email: "bob@example.com", Enabled: false},
				{FeatureName: "cc", Email: "cindy@example.com", Enabled: true},
				{FeatureName: "cc", Email: "danny@example.com", Enabled: false},
			} {
				db.Create(&row)
			}

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/feature", nil)
			q := req.URL.Query()
			for k, v := range map[string]string{
				"email":       tc.email,
				"featureName": tc.featureName,
			} {
				q.Add(k, v)
			}
			req.URL.RawQuery = q.Encode()
			r.ServeHTTP(w, req)

			assert.Equal(t, http.StatusOK, w.Code)
			body := featureGetResponse{}
			if ok := assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &body)); !ok {
				return
			}
			assert.Equal(t, tc.expect, body.CanAccess)
		})
	}
}

func TestGetInvalidQuery(t *testing.T) {
	testCases := []struct {
		desc  string
		query map[string]string
	}{
		{
			"empty query",
			map[string]string{},
		},
		{
			"missing email",
			map[string]string{
				"featureName": "abc",
			},
		},
		{
			"missing featureName",
			map[string]string{
				"email": "foo@example.com",
			},
		},
		{
			"empty email",
			map[string]string{
				"email":       "",
				"featureName": "abc",
			},
		},
		{
			"empty featureName",
			map[string]string{
				"email":       "foo@example.com",
				"featureName": "",
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			r, _, err := setup()
			if ok := assert.Nil(t, err); !ok {
				return
			}

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/feature", nil)
			q := req.URL.Query()
			for k, v := range tc.query {
				q.Add(k, v)
			}
			req.URL.RawQuery = q.Encode()
			r.ServeHTTP(w, req)

			assert.Equal(t, http.StatusBadRequest, w.Code)
			body := errorResponse{}
			if ok := assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &body)); !ok {
				return
			}
			assert.Equal(t, http.StatusBadRequest, body.Error.Code)
		})
	}
}

func TestPost(t *testing.T) {
	testCases := []struct {
		desc       string
		expect     models.StorableSwitch
		expectCode int
	}{
		{
			"feature exists, email not exists",
			models.StorableSwitch{FeatureName: "aa", Email: "zzz@example.com", Enabled: true},
			http.StatusOK,
		},
		{
			"feature not exists, email exists",
			models.StorableSwitch{FeatureName: "zz", Email: "alan@example.com", Enabled: true},
			http.StatusOK,
		},
		{
			"feature doesn't have email record",
			models.StorableSwitch{FeatureName: "aa", Email: "cindy@example.com", Enabled: true},
			http.StatusOK,
		},
		{
			"feature disable the enabled",
			models.StorableSwitch{FeatureName: "aa", Email: "alan@example.com", Enabled: false},
			http.StatusOK,
		},
		{
			"feature enable the enabled",
			models.StorableSwitch{FeatureName: "aa", Email: "alan@example.com", Enabled: true},
			http.StatusNotModified,
		},
		{
			"feature enable the disabled",
			models.StorableSwitch{FeatureName: "aa", Email: "bob@example.com", Enabled: true},
			http.StatusOK,
		},
		{
			"feature disable the disabled",
			models.StorableSwitch{FeatureName: "aa", Email: "bob@example.com", Enabled: false},
			http.StatusNotModified,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			r, db, err := setup()
			if ok := assert.Nil(t, err); !ok {
				return
			}
			for _, row := range []models.StorableSwitch{
				{FeatureName: "aa", Email: "alan@example.com", Enabled: true},
				{FeatureName: "aa", Email: "bob@example.com", Enabled: false},
				{FeatureName: "cc", Email: "cindy@example.com", Enabled: true},
				{FeatureName: "cc", Email: "danny@example.com", Enabled: false},
			} {
				db.Create(&row)
			}

			payload, err := json.Marshal(gin.H{
				"featureName": tc.expect.FeatureName,
				"email":       tc.expect.Email,
				"enable":      tc.expect.Enabled,
			})
			if ok := assert.Nil(t, err); !ok {
				return
			}
			w := httptest.NewRecorder()
			req, _ := http.NewRequest("POST", "/feature", bytes.NewBuffer(payload))
			r.ServeHTTP(w, req)

			assert.Equal(t, tc.expectCode, w.Code)
			if tc.expectCode == http.StatusNotModified {
				// gin framework only allows returning empty body for HTTP 100-199,204,304
				// refer: gin/context.go:bodyAllowedForStatus
				assert.Equal(t, "", w.Body.String())
			} else {
				body := gin.H{}
				if ok := assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &body)); !ok {
					return
				}
				assert.Equal(t, gin.H{}, body)
			}

			var row models.StorableSwitch
			result := db.First(&row, tc.expect)
			assert.Nil(t, result.Error)
			assert.NotNil(t, row)
			assert.Equal(t, tc.expect.Enabled, row.Enabled)
		})
	}
}

func TestPostInvalidJson(t *testing.T) {
	testCases := []struct {
		desc string
		req  gin.H
	}{
		{
			"negative: empty JSON",
			gin.H{},
		},
		{
			"negative: missing featureName",
			gin.H{"email": "foo@example.com", "enable": true},
		},
		{
			"negative: missing email",
			gin.H{"featureName": "abc", "enable": true},
		},
		{
			"negative: missing enable",
			gin.H{"featureName": "abc", "email": "foo@example.com"},
		},
		{
			"negative: empty featureName",
			gin.H{"featureName": "", "email": "foo@example.com", "enable": true},
		},
		{
			"negative: empty email",
			gin.H{"featureName": "abc", "email": "", "enable": true},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			r, _, err := setup()
			if ok := assert.Nil(t, err); !ok {
				return
			}

			payload, err := json.Marshal(tc.req)
			if ok := assert.Nil(t, err); !ok {
				return
			}
			w := httptest.NewRecorder()
			req, _ := http.NewRequest("POST", "/feature", bytes.NewBuffer(payload))
			r.ServeHTTP(w, req)

			assert.Equal(t, http.StatusBadRequest, w.Code)
			body := errorResponse{}
			if ok := assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &body)); !ok {
				return
			}
			assert.Equal(t, http.StatusBadRequest, body.Error.Code)
		})
	}
}

func TestPostNotJson(t *testing.T) {
	r, _, err := setup()
	if ok := assert.Nil(t, err); !ok {
		return
	}

	payload := []byte{}
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/feature", bytes.NewBuffer(payload))
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
	body := errorResponse{}
	if ok := assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &body)); !ok {
		return
	}
	assert.Equal(t, http.StatusBadRequest, body.Error.Code)
}
