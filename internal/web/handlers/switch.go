package handlers

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"
	"gitlab.com/MachHach/ml-feature-service/internal/database/dbrepos"
	"gitlab.com/MachHach/ml-feature-service/internal/models"
	"gitlab.com/MachHach/ml-feature-service/internal/utils"
	"gorm.io/gorm"
)

func GetFeatureSwitch(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var input models.SwitchGetQuery
		if err := c.ShouldBindQuery(&input); err != nil {
			utils.AbortJsonFromBindQuery(c, err)
			return
		}

		canAccess := false
		st, err := dbrepos.GetSwitch(db, input.FeatureName, input.Email)
		if err != nil && !utils.IsErrorDatabaseRecordNotFound(err) {
			// unexpected error
			msg := "unable to get feature"
			log.WithFields(log.Fields{"err": err}).Error(msg)
			utils.AbortJsonFromErrorString(c, http.StatusInternalServerError, msg)
			return
		}
		// if no record found, `st` is empty-valued hence `st.Enabled` is false
		canAccess = st.Enabled

		c.JSON(http.StatusOK, gin.H{
			"canAccess": canAccess,
		})
	}
}

func CreateFeatureSwitch(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var input models.SwitchCreationJson
		if err := c.ShouldBindJSON(&input); err != nil {
			utils.AbortJsonFromBindJSON(c, err)
			return
		}

		st, err := dbrepos.GetSwitch(db, input.FeatureName, input.Email)
		if err != nil {
			if utils.IsErrorDatabaseRecordNotFound(err) {
				// doesn't exist yet, to create
				if _, err2 := dbrepos.CreateSwitch(db, input); err2 != nil {
					msg := "unable to create feature"
					log.WithFields(log.Fields{"err": err}).Error(msg)
					utils.AbortJsonFromErrorString(c, http.StatusInternalServerError, msg)
					return
				}
				c.JSON(http.StatusOK, gin.H{})
				return
			} else {
				// unexpected error
				msg := "unable to get feature"
				log.WithFields(log.Fields{"err": err}).Error(msg)
				utils.AbortJsonFromErrorString(c, http.StatusInternalServerError, msg)
				return
			}
		}

		if st.Enabled == *input.Enabled {
			// existing switch's status is same as requested
			c.JSON(http.StatusNotModified, gin.H{})
			return
		}

		if _, err := dbrepos.UpdateSwitch(db, st, *input.Enabled); err != nil {
			// unexpected error
			msg := "unable to update feature"
			log.WithFields(log.Fields{"err": err}).Error(msg)
			utils.AbortJsonFromErrorString(c, http.StatusInternalServerError, msg)
			return
		}

		c.JSON(http.StatusOK, gin.H{})
	}
}
