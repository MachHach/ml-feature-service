package database

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/MachHach/ml-feature-service/internal/config"
)

func TestNewMySqlConfig(t *testing.T) {
	appCfg := config.MySqlConfig{
		Address:  "addr",
		Username: "un",
		Password: "pw",
		Database: "db",
	}
	mySqlCfg := NewMySqlConfig(&appCfg)
	assert.Equal(t, appCfg.Username, mySqlCfg.User)
	assert.Equal(t, appCfg.Password, mySqlCfg.Passwd)
	assert.Equal(t, "tcp", mySqlCfg.Net)
	assert.Equal(t, appCfg.Address, mySqlCfg.Addr)
	assert.Equal(t, appCfg.Database, mySqlCfg.DBName)
	assert.Equal(t, true, mySqlCfg.ParseTime)
	assert.Equal(t, map[string]string{"charset": "utf8mb4"}, mySqlCfg.Params)
}
