package dbrepos

import (
	"gitlab.com/MachHach/ml-feature-service/internal/models"
	"gitlab.com/MachHach/ml-feature-service/internal/transform"
	"gorm.io/gorm"
)

func GetSwitch(db *gorm.DB, fname string, email string) (models.StorableSwitch, error) {
	var row models.StorableSwitch
	result := db.First(&row, models.StorableSwitch{FeatureName: fname, Email: email})
	return row, result.Error
}

func CreateSwitch(db *gorm.DB, input models.SwitchCreationJson) (uint, error) {
	row := transform.InputToStorableSwitch(input)
	result := db.Create(&row)
	return row.ID, result.Error
}

func UpdateSwitch(db *gorm.DB, st models.StorableSwitch, enabled bool) (int64, error) {
	result := db.Model(&st).Where(&st).Updates(map[string]interface{}{"enabled": enabled})
	return result.RowsAffected, result.Error
}
