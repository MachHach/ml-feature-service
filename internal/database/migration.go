package database

import (
	"gitlab.com/MachHach/ml-feature-service/internal/models"
	"gorm.io/gorm"
)

func AutoMigrate(db *gorm.DB) error {
	return db.AutoMigrate(
		&models.StorableSwitch{},
	)
}
