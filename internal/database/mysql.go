package database

import (
	"fmt"
	"time"

	mysqldriver "github.com/go-sql-driver/mysql"
	"gitlab.com/MachHach/ml-feature-service/internal/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func NewMySqlConfig(appCfg *config.MySqlConfig) *mysqldriver.Config {
	// refer for details: https://github.com/go-sql-driver/mysql#dsn-data-source-name
	cfg := mysqldriver.NewConfig()
	cfg.User = appCfg.Username
	cfg.Passwd = appCfg.Password
	cfg.Net = "tcp"
	cfg.Addr = appCfg.Address
	cfg.DBName = appCfg.Database
	cfg.Loc = time.UTC
	cfg.ParseTime = true
	cfg.Params = map[string]string{"charset": "utf8mb4"}
	return cfg
}

func NewMySqlClient(appCfg *config.MySqlConfig) (db *gorm.DB, err error) {
	mySqlCfg := NewMySqlConfig(appCfg)
	dsn := mySqlCfg.FormatDSN()
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return db, fmt.Errorf("unable to connect to database: %w", err)
	}
	return
}
