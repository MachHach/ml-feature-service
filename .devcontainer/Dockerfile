FROM golang:1.16.13-buster as base

### Development use
FROM base as dev

# Tools
RUN apt-get update -y \
    && apt-get install --no-install-recommends -y git less vim openssh-client curl \
    && rm -rf /var/lib/apt/lists/* \
    && git --version

# Set up non-root user
# https://code.visualstudio.com/docs/remote/containers-advanced#_change-the-uidgid-of-an-existing-container-user

ARG USERNAME=vscode
RUN useradd -ms /bin/bash $USERNAME

ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupmod --gid $USER_GID $USERNAME \
    && usermod --uid $USER_UID --gid $USER_GID $USERNAME \
    && chown -R $USER_UID:$USER_GID /home/$USERNAME

USER $USERNAME

WORKDIR /workspace
